from django.conf.urls import url, include
from rest_framework import routers
from tuturial.quickstart import views



urlpatterns = [

    #url(r'^stats/$', views.stats_list, name='stats_list'),
    url(r'^stats/(?P<pk>[a-zA-Z ]+)$', views.statsForAuthor, name='statsForAuthor'),
    url(r'^stats/$', views.Stats.as_view()),
    #url(r'^stats/(?P<pk>[a-zA-Z ]+)/$', views.StatsForAuthor.as_view()),
]
